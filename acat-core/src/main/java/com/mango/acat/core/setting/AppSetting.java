package com.mango.acat.core.setting;

import com.xiaoleilu.hutool.setting.Setting;

public class AppSetting {
	
	public static Setting app;
	public static final String path = "app.properties";
	
	static{
		app = new Setting(path);
	}
}
