package com.mango.acat.core.server;
/**
 * 服务器接口
 * @author mango 2017-04-07
 *
 */
public abstract interface IServer{
  public abstract void start(int port);
  public abstract void stop();
  public abstract void restart();
}
