package com.mango.acat.core.dataset;

import java.util.Map;

import com.google.common.collect.Maps;
import com.mango.acat.core.store.RequestStore;
import com.mango.acat.core.store.SessionStore;

public class FreemarkerDataSet extends DataSet{
	
	public FreemarkerDataSet(String path) {
		this.setType(FREEMARKER);
		this.setPath(path);
		//获取requestStore
		Map<String,Object> data = RequestStore.getData();
		data.putAll(SessionStore.getData());
		//设置具体session
		if(!SessionStore.getData().isEmpty()){
			this.setSessionData(SessionStore.getData());
		}else{
			this.sessionData = Maps.newHashMap();
		}
		this.setData(data);
	}
	
	private String path;
	private Map<String,Object> data;
	
	private Map<String,Object> sessionData;
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public Map<String, Object> getSessionData() {
		return sessionData;
	}

	public void setSessionData(Map<String, Object> sessionData) {
		this.sessionData = sessionData;
	}
	
	
}
