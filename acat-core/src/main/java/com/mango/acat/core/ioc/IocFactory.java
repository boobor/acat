package com.mango.acat.core.ioc;

import java.util.HashMap;

import com.google.common.collect.Maps;
import com.xiaoleilu.hutool.lang.Singleton;

import freemarker.cache.StringTemplateLoader;

public class IocFactory {
	/**
	 * 请求key容器，<reqkey,class>
	 */
	public static HashMap<String, String> reqKeyMap = Maps.newHashMap();
	
	private static HashMap<String, Object> iocMap = Maps.newHashMap();

	public static Object getObject(Class<StringTemplateLoader> class1) {
		if(iocMap.containsKey(class1)){
			return iocMap.get(class1);
		}else{
			return Singleton.get(class1);
		}
	}
	
}
